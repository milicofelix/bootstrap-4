<?php
require_once("header.php");
?>

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Recuperar senha</div>
        <div class="card-body">
            <div class="text-center mt-4 mb-5">
                <h4>Esqueceu sua senha?</h4>
                <p>Digite seu email e nós lhe enviaremos instruções como redefinir sua senha.</p>
            </div>
            <form>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Digite seu email">
                </div>
                <button class="btn btn-primary btn-block">Recuperar Senha</button>
                <div class="text-center">
                    <a href="#" class="d-block small mt-3">Criar nova Conta</a>
                    <a href="#" class="d-block small">Página de login</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require_once("footer.php");?>