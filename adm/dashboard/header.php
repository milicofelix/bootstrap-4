<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Revisando o Bootstrap 4</title>
    <link rel="stylesheet" href="bibliotecas/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bibliotecas/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/sb-admin.min.css">
</head>
<body class="bg-dark">