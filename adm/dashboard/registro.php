<?php
require_once("header.php");
?>

<div class="container">
    <div class="card card-register mx-auto mt-5">
        <div class="card-header">Criar uma Conta</div>
        <div class="card-body">
            <form>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="primeiro_nome">Primeiro Nome</label>
                            <input type="text" name="primeiro_nome" id="primeiro_nome" class="form-control" placeholder="Digite seu primeiro nome">
                        </div>
                        <div class="col-md-6">
                            <label for="sobre_nome">Sobrenome</label>
                            <input type="text" name="sobre_nome" id="sobre_nome" class="form-control" placeholder="Digite seu sobrenome">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Digite seu email">
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" name="senha" id="senha" placeholder="Digite uma senha">
                        </div>
                        <div class="col-md-6">
                            <label for="confirmar_senha">Confirmar Senha</label>
                            <input type="password" class="form-control" name="confirmar_senha" id="confirmar_senha" placeholder="Confirmar a senha">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-block">Registrar-se</button>
                <div class="text-center">
                    <a href="#" class="d-block small mt-3">Esqueceu a senha?</a>
                    <a href="#" class="d-block small">Página de login</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require_once("footer.php");?>