<?php
require_once("header.php");
?>

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form action="" method="post">
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Digite seu email">
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <input type="password" class="form-control" name="senha" id="senha" placeholder="Digite sua senha">
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" name="" id="">
                            lembrar minha senha
                        </label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block">Entrar</button>
                <div class="text-center">
                    <a href="#" class="d-block small mt-3">Criar nova Conta</a>
                    <a href="#" class="d-block small">Esqueci minha senha?</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require_once("footer.php");?>