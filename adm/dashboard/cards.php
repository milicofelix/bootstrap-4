<?php
require_once("admin_header.php");
?>
<!--Navegação-->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a href="" class="navbar-brand">Admin</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarAdmin"
    aria-controls="navbarAdmin" aria-expanded="false" aria-label="Navegação toggle">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarAdmin">
        <ul class="navbar-nav navbar-sidenav" id="linksaccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right">
                <a href="" class="nav-link">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Dashboard</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right">
                <a href="tabelas.php" class="nav-link">
                    <i class="fa fa-fw fa-table"></i>
                    <span class="nav-link-text">Tabelas</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right">
                <a href="#linkscomponentes" class="nav-link nav-link-collapse collapse" data-toggle="collapse" data-parent="#linksaccordion">
                    <i class="fa fa-fw fa-wrench"></i>
                    <span class="nav-link-text">Componentes</span>
                </a>
                <ul class="sidenav-second-level collapse" id="linkscomponentes">
                    <li>
                        <a href="javascrip:void(0)">Componente-1</a>
                    </li>
                    <li>
                        <a href="javascrip:void(0)">Componente-2</a>
                    </li>
                    <li>
                        <a href="javascrip:void(0)">Componente-3</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="#linkspagina" class="nav-link nav-link-collapse collapse" data-toggle="collapse" data-parent="#linksaccordion" >
                    <i class="fa fa-fw fa-file"></i>
                    <span class="nav-link-text">Páginas</span>
                </a>
                <ul class="sidenav-second-level collapse" id="linkspagina">
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <li>
                        <a href="recuperar.php">Recuperar Senha</a>
                    </li>
                    <li>
                        <a href="registro.php">Registrar</a>
                    </li>

                </ul>
            </li>
        </ul>
        <ul class="navbar-nav sidenav-toggler">
            <li class="nav-item">
                <a href="#" class="nav-link text-center" id="sidenavToggler">
                    <i class="fa fa-fw fa-angle-left"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a href="" class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="d-lg-none">
                        Mensagens
                    <span class="badge badge-pill badge-primary">12 novas</span>
                    </span>
                    <span class="indicator text-primary d-none d-lg-block">
                        <i class="fa fa-fw fa-circle"></i>
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">Novas Mensagens</h6>
                    <div class="dropdown-divider"></div>
                    <a href="" class="dropdown-item">
                        <strong>João do Ratos</strong>
                        <span class="small float-right text-muted">15:08</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                        <div class="dropdown-divider"></div>
                    </a>
                    <a href="" class="dropdown-item">
                        <strong>Adriano Felix</strong>
                        <span class="small float-right text-muted">15:08</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                        <div class="dropdown-divider"></div>
                    </a>
                    <a href="" class="dropdown-item">
                        <strong>Claudemir Lins</strong>
                        <span class="small float-right text-muted">13:18</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                        <div class="dropdown-divider"></div>
                    </a>
                    <a href="" class="dropdown-item">
                        <strong>Jonathan Xavier</strong>
                        <span class="small float-right text-muted">11:04</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item small">Ver Todas as mensagens</a>
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a href="" class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-fw fa-bell"></i>
                    <span class="d-lg-none">
                        Alertas
                    <span class="badge badge-pill badge-warning">5 novos</span>
                    </span>
                    <span class="indicator text-warning d-none d-lg-block">
                        <i class="fa fa-fw fa-circle"></i>
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="alertsDropdown">
                    <h6 class="dropdown-header">Novos Alertas</h6>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <span class="text-success">
                            <strong>
                                <i class="fa fa-fw fa-long-arrow-up"></i>
                                Atualização de estado
                            </strong>
                        </span>
                        <span class="small float-right text-muted">12:02</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <span class="text-danger">
                            <strong>
                                <i class="fa fa-fw fa-long-arrow-down"></i>
                                Atualização de estado
                            </strong>
                        </span>
                        <span class="small float-right text-muted">18:28</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <span class="text-success">
                            <strong>
                                <i class="fa fa-fw fa-long-arrow-up"></i>
                                Atualização de estado
                            </strong>
                        </span>
                        <span class="small float-right text-muted">22:08</span>
                        <div class="dropdown-message small">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem minima recusandae tempora veritatis. A, asperiores cum dolor dolorem ea eveniet illo ipsa libero officiis quae quis quisquam ullam ut veritatis.
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item small">Ver Todas as mensagens</a>
                </div>
            </li>
            <li class="nav-item">
                <form class="form-inline my-2 my-lg-0 mr-lg-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Pesquisar...">
                        <span class="input-group-btn"></span>
                        <button class="btn btn-primary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </form>
            </li>
            <li class="nav-item">
                <a href="login.php" class="nav-link">
                    <i class="fa fa-sign-out">Sair</i>
                </a>
            </li>
        </ul>
    </div>
</nav>
    <div class="content-wrapper">
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="">Home</a>
                </li>
                <li class="breadcrumb-item">
                    Cards
                </li>
            </ol>
            <div class="row">
                <div class="col-12">
                    <h1>Cards</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fw fa-comments"></i>
                            </div>
                            <div class="mr-5">
                                30 mensagens!
                            </div>
                        </div>
                        <a href="#" class="card-footer text-white clearfix small z-1">
                            <span class="float-left">
                                Ver Detalhes
                            </span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>

                    </div>

                </div>

                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-warning o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fw fa-list"></i>
                            </div>
                            <div class="mr-5">
                                10 Tarefas!
                            </div>
                        </div>
                        <a href="#" class="card-footer text-white clearfix small z-1">
                            <span class="float-left">
                                Ver Detalhes
                            </span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>

                    </div>

                </div>

                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fw fa-shopping-cart"></i>
                            </div>
                            <div class="mr-5">
                                234 Vendas!
                            </div>
                        </div>
                        <a href="#" class="card-footer text-white clearfix small z-1">
                            <span class="float-left">
                                Ver Detalhes
                            </span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>

                    </div>

                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-danger o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fw fa-support"></i>
                            </div>
                            <div class="mr-5">
                                20 Tickets!
                            </div>
                        </div>
                        <a href="#" class="card-footer text-white clearfix small z-1">
                            <span class="float-left">
                                Ver Detalhes
                            </span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>

                    </div>

                </div>
            </div>
        </div>
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright nome site 2020</small>
                </div>
            </div>
        </footer>
    </div>

<?php require_once("admin_footer.php");?>