<?php require_once "header.php";?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h3>Leitura do hidrômetro -  Sabesp</h3>
            <form action="">
                <div class="form-inline">
                    <label for="leitura1" class="text-info col-sm-2 mb-2 mr-2">Leitura atual</label>
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura1" name="leitura1">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura1_1" name="leitura1_1">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura1_2" name="leitura1_2">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura1_3" name="leitura1_3">
                </div>
                <div class="form-inline">
                    <label for="leitura2" class="text-info col-sm-2 mb-2 mr-2">Leitura anterior</label>
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura2" name="leitura2">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura2_1" name="leitura2_1">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura2_2" name="leitura2_2">
                    <input type="text" maxlength="1" class="form-control col-sm-1 mb-2 mr-2" id="leitura2_3" name="leitura2_3">
                </div>
                <div class="form-check col-sm-3">
                    <div class="input-group">
                        <label for="taxa" class="form-check-label">Tx Regulação -  TRCF</label>
                        <span class="input-group-addon">
                             <input type="checkbox" class="form-check-input">
                        </span>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Gravar</button>
            </form>
        </div>
    </div>
</div>

<?php require_once "footer.php";?>